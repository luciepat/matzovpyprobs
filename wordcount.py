import sys


def print_words(filename):
    word_dic = get_frequency_dic(filename)
    for key in sorted(word_dic.keys()):
        print("Word:{} Appears:{}".format(key, word_dic[key]))


def print_top(filename):
    word_dic = get_frequency_dic(filename)
    keys_by_freq = sorted(word_dic, key=word_dic.get, reverse=True)
    for i in range(0, 20):
        print("Word:{} Appears:{}".format(keys_by_freq[i], word_dic[keys_by_freq[i]]))


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


def open_file(filename):
    """Takes filename and returns a list of all words in file"""
    text = ""
    with open(filename, "r") as f:
        if not (f.readable()):
            print("Input is not text file")
            sys.exit(1)
        text = f.read()
    return text.split()


def get_frequency_dic(filename):
    """Gets filename and returns a frequency dictionary"""
    freq_dic = {}
    text = open_file(filename)
    for word in text:
        low_word = word.lower()
        split = low_word.split('--')# makes case insensitive
        for split_word in split:
            if split_word != "" and split_word in freq_dic.keys():  # if the word exists,add 1
                freq_dic[split_word] += 1
            elif split_word != "":
                freq_dic[split_word] = 1
    return freq_dic


if __name__ == '__main__':
    main()
